image: registry.gitlab.com/thorchain/thornode:builder-v4@sha256:a58b06a98485bcef78d7733cc6d66e8c62a306b1ec388a032469c592c5a71841

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_REF_PROTECTED == "true"
    - if: $CI_PIPELINE_SOURCE == "api"
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_PIPELINE_SOURCE == "webide"

variables:
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: /certs
  DOCKER_TLS_VERIFY: 1
  DOCKER_CERT_PATH: $DOCKER_TLS_CERTDIR/client
  SAST_EXCLUDED_ANALYZERS: eslint,bandit,semgrep
  SAST_EXCLUDED_PATHS: spec, test, tests, tmp, .cache

stages:
  - fork
  - reset
  - generate
  - test
  - build
  - long-test

fork:
  stage: fork
  rules:
    - if: $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH && $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH != "thorchain/thornode"
      when: always
  script:
    - |
      if ! curl -s https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines?sha=$CI_COMMIT_SHA | jq -e '.[]|select(.status=="success")'; then
        echo '1. Ensure the following passes locally: `make generate lint test smoke test-regression`'
        echo '2. Have a contributor run `make gitlab-trigger-ci`.'
        echo '3. Re-run this job.'
        exit 1
      fi

reset-lint-cache:
  cache:
    key: lint
    paths:
      - .cache
  stage: reset
  when: manual
  script:
    - find .cache -maxdepth 2 || true
    - find .cache -mindepth 1 -delete
  allow_failure: true

generate:
  rules: &rules
    - if: $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH && $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH != "thorchain/thornode"
      when: never
    - if: $CI == "true"
      when: on_success
  stage: generate
  cache: {}
  image: docker:20.10.12
  services:
    - docker:20.10.12-dind
  artifacts:
    paths:
      - test/smoke/thornode_proto
  before_script:
    - apk -U add bash curl findutils git go jq make protoc sed wget
  script:
    - PATH="$PATH:/root/go/bin" make generate smoke-protob-docker
    - ./scripts/lint-generated.bash

unit-tests:
  rules: *rules
  stage: test
  coverage: /total:\s+\(statements\)\s+(\d+.\d+\%)/
  before_script:
    - go get github.com/boumenot/gocover-cobertura
    - go get gotest.tools/gotestsum
  script:
    - make test-coverage-sum
  artifacts:
    when: always
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - coverage*

lint:
  rules:
    - if: $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH && $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH != "thorchain/thornode"
      when: never
    - if: $CI_COMMIT_BRANCH == "testnet"
      when: never
    - if: $CI_COMMIT_BRANCH == "stagenet"
      when: never
    - if: $CI_COMMIT_BRANCH == "mainnet"
      when: never
    - if: $CI == "true"
      when: on_success
  stage: test
  cache:
    key: lint
    paths:
      - .cache
  before_script:
    - rm -rf ~/.cache
    - mkdir -p .cache
    - mv .cache ~/.cache
    - git fetch origin develop
  script:
    - make lint-ci
  after_script:
    - ./scripts/trunk cache prune
    - rm -rf .cache
    - mv ~/.cache .

build-thornode:
  rules:
    - if: $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH && $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH != "thorchain/thornode"
      when: never
    - if: $CI_COMMIT_BRANCH == "develop"
      when: always
    - if: $CI_COMMIT_BRANCH == "testnet"
      when: always
    - if: $CI_COMMIT_BRANCH == "stagenet"
      when: always
    - if: $CI_COMMIT_BRANCH == "mainnet"
      when: always
  stage: build
  image: docker:20.10.12
  services:
    - docker:20.10.12-dind
  before_script:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384#note_497228752
    - |
      for i in $(seq 1 30)
      do
          docker info && break
          echo "Waiting for docker to start"
          sleep 1s
      done
    - apk -U add make git jq curl protoc
    - make docker-gitlab-login
  script:
    - make docker-gitlab-build
    - make docker-gitlab-push
    # also push mocknet from develop
    - BUILDTAG=mocknet BRANCH=mocknet make docker-gitlab-build
    - BUILDTAG=mocknet BRANCH=mocknet make docker-gitlab-push

build-smoke:
  rules:
    - if: $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH && $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH != "thorchain/thornode"
      when: never
    - if: $CI_COMMIT_BRANCH == "develop"
      when: always
  stage: build
  image: docker:20.10.12
  services:
    - docker:20.10.12-dind
  before_script:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384#note_497228752
    - |
      for i in $(seq 1 30)
      do
          docker info && break
          echo "Waiting for docker to start"
          sleep 1s
      done
    - apk -U add make git jq curl protoc
    - make docker-gitlab-login
  script:
    - make smoke-build-image
    - make smoke-push-image

smoke-test:
  rules: *rules
  stage: long-test
  cache: {}
  image: docker:20.10.12
  services:
    - docker:20.10.12-dind
  artifacts:
    when: on_failure
    name: $CI_JOB_NAME-$CI_COMMIT_REF_NAME
    paths:
      - ./logs/
  environment:
    name: integration
  variables:
    MIDGARD_REPO: https://gitlab.com/thorchain/midgard.git
    MIDGARD_IMAGE: registry.gitlab.com/thorchain/midgard:develop
    THOR_BLOCK_TIME: 0.5s
    BLOCK_TIME: "2"
    BLOCK_SCANNER_BACKOFF: 0.3s
  before_script:
    - apk -U add git make protoc bash
    - (git ls-remote $MIDGARD_REPO $CI_COMMIT_REF_NAME && git clone --single-branch -b $CI_COMMIT_REF_NAME $MIDGARD_REPO && cd ./midgard && IMAGE_NAME=$MIDGARD_IMAGE make build && cd ..) || (git ls-remote $MIDGARD_REPO develop && git clone --single-branch -b develop $MIDGARD_REPO && docker pull $MIDGARD_IMAGE)
    - |
      PLUGIN="$HOME/.docker/cli-plugins/docker-compose"
      mkdir -p $(dirname $PLUGIN)
      wget https://github.com/docker/compose/releases/download/v2.11.1/docker-compose-linux-x86_64 -O $PLUGIN
      chmod +x $PLUGIN

  script:
    - make smoke
  after_script:
    - mkdir logs
    - docker ps -a >./logs/ps.log 2>&1
    - |
      for name in $(docker ps --format '{{.Names}}'); do
        docker logs "$name" >"./logs/$name.log" 2>&1
      done

test-regression:
  rules: *rules
  stage: long-test
  cache: {}
  image: docker:20.10.12
  services:
    - docker:20.10.12-dind
  environment:
    name: integration
  variables:
    DOCKER_BUILDKIT: "1"
    TIME_FACTOR: "2"
    PARALLELISM: "8"
  before_script:
    - apk -U add make git bash
  script:
    - make test-regression

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml

# NOTE: The following included jobs have internal rule definitions that need to be
# overwritten for them to trigger on merge requests. We overwrite all with the default
# workflow rule set.

secret_detection:
  stage: test
  rules: *rules

semgrep:
  stage: test
  rules: *rules
  image: returntocorp/semgrep-agent:v1
  script: semgrep-agent --gitlab-json > gl-sast-report.json || true
  variables:
    SEMGREP_RULES: >-
      https://semgrep.dev/s/heimdallthor:insecure-logging
  artifacts:
    reports:
      sast: gl-sast-report.json
